`timescale 1 ns / 1 ps

module system (
	input            clk,
	input            resetn,
	output           trap,
	output reg [31:0] out_byte,
	output reg       out_byte_en
);
	// set this to 0 for better timing but less performance/MHz
	parameter FAST_MEMORY = 0;

	// 4096 32bit words = 16kB memory
//	parameter MEM_SIZE = 4096;
	parameter MEM_SIZE = 16384;
	wire mem_valid;
	wire mem_instr;
	reg mem_ready;
	wire [31:0] mem_addr;
	wire [31:0] mem_wdata;
	wire [3:0] mem_wstrb;
	reg [31:0] mem_rdata;
	
	wire mem_la_read;
	wire mem_la_write;
	wire [31:0] mem_la_addr;
	wire [31:0] mem_la_wdata;
	wire [3:0] mem_la_wstrb;


    //-------
    reg [15:0] in_mulA;
    reg [15:0] in_mulB;    
    reg        ini;
    reg [15:0] in_fact;
    wire [15:0] out_fact;
    wire       fact_done;
    wire [31:0] out_mul;
	wire [31:0] out_mux;
	reg display;

//multiplicador #(.SIZE(16)) multi (
//	   .data_out   (out_mul),
//	   .inA        (in_mulA),
//	   .inB        (in_mulB)
//);

//factorial fact ( 
//    .clk    (clk),
//    .reset  (resetn),
//    .ini    (ini),
//    .fact   (in_fact),
//    .out_result (out_fact),
//    .done   (fact_done)
//);

//mux_mult_varbits mux (
//    .A  (in_mulA),
//    .B  (in_mulB),
//    .data_out   (out_mux)
//);
//fsm fact(
//    .clk (clk),
//    .reset (resetn),
//    .in_fact (in_fact),
//    .go (ini),
//    .fact (out_fact),
//    .done (fact_done)
//);

	wire cpu_valid;
	wire cpu_instr;
	wire cpu_ready;
	wire [31:0] cpu_addr;
	wire [31:0] cpu_wdata;
	wire [3:0] cpu_wstrb;
	wire [31:0] cpu_rdata;
	wire [3:0] state;
	wire [3:0] next_state;
	

//picorv32 //#(.ENABLE_MUL(1)) 
//picorv32_core (
//		.clk         (clk         ),
//		.resetn      (resetn      ),
//		.trap        (trap        ),
//		.mem_valid   (mem_valid   ),
//		.mem_instr   (mem_instr   ),
//		.mem_ready   (mem_ready   ),
//		.mem_addr    (mem_addr    ),
//		.mem_wdata   (mem_wdata   ),
//		.mem_wstrb   (mem_wstrb   ),
//		.mem_rdata   (mem_rdata   ),
//		.mem_la_read (mem_la_read ),
//		.mem_la_write(mem_la_write),
//		.mem_la_addr (mem_la_addr ),
//		.mem_la_wdata(mem_la_wdata),
//		.mem_la_wstrb(mem_la_wstrb)
//	);
	picorv32 //#(.ENABLE_MUL(1)) 
picorv32_core (
		.clk         (clk         ),
		.resetn      (resetn      ),
		.trap        (trap        ),
		.mem_valid   (cpu_valid   ),
		.mem_instr   (cpu_instr   ),
		.mem_ready   (cpu_ready   ),
		.mem_addr    (cpu_addr    ),
		.mem_wdata   (cpu_wdata   ),
		.mem_wstrb   (cpu_wstrb   ),
		.mem_rdata   (cpu_rdata   ),
		.mem_la_read (mem_la_read ),
		.mem_la_write(mem_la_write),
		.mem_la_addr (mem_la_addr ),
		.mem_la_wdata(mem_la_wdata),
		.mem_la_wstrb(mem_la_wstrb)
	);
	
cache cache1 (
        .clk (clk),
        .resetn (resetn),
        .cpu_valid (cpu_valid),
        .cpu_instr (cpu_instr),
        .cpu_addr  (cpu_addr),
        .cpu_wdata (cpu_wdata),
        .cpu_strb  (cpu_wstrb),
        .mem_ready (mem_ready),
        .mem_rdata (mem_rdata),
        .cpu_ready (cpu_ready),
        .cpu_rdata (cpu_rdata),
        .mem_valid (mem_valid),
        .mem_instr (mem_instr),
        .mem_addr  (mem_addr),
        .mem_wdata (mem_wdata),
        .mem_strb  (mem_wstrb),
        .state     (state),
        .next_state (next_state)
);
//cache4 cache1 (
//        .clk (clk),
//        .resetn (resetn),
//        .cpu_valid (cpu_valid),
//        .cpu_instr (cpu_instr),
//        .cpu_addr  (cpu_addr),
//        .cpu_wdata (cpu_wdata),
//        .cpu_strb  (cpu_wstrb),
//        .mem_ready (mem_ready),
//        .mem_rdata (mem_rdata),
//        .cpu_ready (cpu_ready),
//        .cpu_rdata (cpu_rdata),
//        .mem_valid (mem_valid),
//        .mem_instr (mem_instr),
//        .mem_addr  (mem_addr),
//        .mem_wdata (mem_wdata),
//        .mem_strb  (mem_wstrb),
//        .state     (state),
//        .next_state (next_state)
//);
	//cntrlr_led cntrlr_1 (
		//.out_byte (out_byte),
		//.clk		(clk),
		//.resetn 	(resetn),
		//.cathodes 	(cathodes),
		//.anodes 	(anodes)
	//);
	
	
	reg [31:0] memory [0:MEM_SIZE-1];

`ifdef SYNTHESIS
    initial $readmemh("../firmware/firmware.hex", memory);
`else
	initial $readmemh("firmware.hex", memory);
`endif

	reg [31:0] m_read_data;
	reg m_read_en;

	generate if (FAST_MEMORY) begin
		always @(posedge clk) begin
			mem_ready <= 1;
			out_byte_en <= 0;
			mem_rdata <= memory[mem_la_addr >> 2];
			if (mem_la_write && (mem_la_addr >> 2) < MEM_SIZE) begin
				if (mem_la_wstrb[0]) memory[mem_la_addr >> 2][ 7: 0] <= mem_la_wdata[ 7: 0];
				if (mem_la_wstrb[1]) memory[mem_la_addr >> 2][15: 8] <= mem_la_wdata[15: 8];
				if (mem_la_wstrb[2]) memory[mem_la_addr >> 2][23:16] <= mem_la_wdata[23:16];
				if (mem_la_wstrb[3]) memory[mem_la_addr >> 2][31:24] <= mem_la_wdata[31:24];
			end
			else
			if (mem_la_write && mem_la_addr == 32'h1000_0000) begin
				out_byte_en <= 1;
				out_byte <= mem_la_wdata;
			end
//			if(mem_valid && mem_ready && | mem_wstrb && mem_addr>'hf000) begin
//			     out_byte_en<=1;
//			     out_byte <= mem_la_wdata;
//			end
			if(mem_valid && | mem_wstrb && mem_wdata ==65499)  begin
                display <=1;
                mem_ready <= 1;
            end
            if(mem_valid && display ==1 && mem_addr > 'h4000)  begin
                out_byte <= mem_wdata ;
                display <=0;
            end
//			if (mem_la_write && mem_la_addr == 32'h0FFF_FFF0) begin
//			     //in_mulA <= mem_la_wdata;
//			     in_fact <= mem_la_wdata;
			 
//			end
//			if (mem_la_write && mem_la_addr == 32'h0FFF_FFF4) begin
//			     //in_mulB <= mem_la_wdata;
//			     ini <= mem_la_wdata;
//			end
//			if ( mem_la_read && mem_la_addr == 32'h0FFF_FFFC) begin   
//			     //mem_rdata <= out_fact;
//			     mem_rdata <= fact_done;
//			end
//			if ( mem_la_read && mem_la_addr == 32'h0FFF_FFF8) begin
//			     mem_rdata <= out_fact;
//			end
			
		end
	end else begin
		always @(posedge clk) begin
			m_read_en <= 0;
			mem_ready <= mem_valid && !mem_ready && m_read_en;

			m_read_data <= memory[mem_addr >> 2];
			mem_rdata <= m_read_data;

			out_byte_en <= 0;

			(* parallel_case *)
			case (1)
				mem_valid && !mem_ready && !mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					m_read_en <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					if (mem_wstrb[0]) memory[mem_addr >> 2][ 7: 0] <= mem_wdata[ 7: 0];
					if (mem_wstrb[1]) memory[mem_addr >> 2][15: 8] <= mem_wdata[15: 8];
					if (mem_wstrb[2]) memory[mem_addr >> 2][23:16] <= mem_wdata[23:16];
					if (mem_wstrb[3]) memory[mem_addr >> 2][31:24] <= mem_wdata[31:24];
					mem_ready <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && mem_addr == 32'h1000_0000: begin
					out_byte_en <= 1;
					out_byte <= mem_wdata;
					mem_ready <= 1;
				end
			endcase
		end
	end endgenerate
endmodule

